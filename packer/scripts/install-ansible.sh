#!/usr/bin/env bash

# https://github.com/phusion/baseimage-docker/issues/58#issuecomment-48032250
echo 'debconf debconf/frontend select Noninteractive' | sudo debconf-set-selections

echo "**********************************************"
echo "Update APT packages"
echo "**********************************************"
sudo apt-get update

echo "**********************************************"
echo "Install Git latest version"
echo "**********************************************"
sudo add-apt-repository --yes --update ppa:git-core/ppa
sudo apt-get -qq update
sudo apt-get -qq install --yes git
git --version

echo "**********************************************"
echo "Install Ansible"
echo "**********************************************"
# Avoid 'Could not find aptitude. Using apt-get instead' message
sudo apt-get install --yes aptitude

# Avoid 'Could not find aptitude. Using apt-get instead'
sudo apt-get install --yes python-apt

sudo apt-get install --yes software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt-get -qq update
sudo apt-get install --yes ansible
ansible --version
